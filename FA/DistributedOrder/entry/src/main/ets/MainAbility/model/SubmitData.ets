/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License,Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * 列表分布式数据库
 */

import CommonLog from './CommonLog';
import distributedData from '@ohos.data.distributedData';

const STORE_ID = 'finish';

export class SubmitData {
  callback;
  kvManager;
  kvStore;
  dataItem;

  constructor() {
  }

  createKvStore(callback) {
    if ((typeof (this.kvStore) !== 'undefined')) {
      CommonLog.info('[KvStoreModel] kvStore has exist');
      callback();
      return;
    }
    var config = {
      bundleName: 'com.distributed.order',
      userInfo: {
        userId: '0',
        userType: 0
      }
    };
    let self = this;
    CommonLog.info('[KvStoreModel] createKVManager begin');
    distributedData.createKVManager(config).then((manager) => {
      CommonLog.info('[KvStoreModel] createKVManager success, kvManager=' + JSON.stringify(manager));
      self.kvManager = manager;
      let options = {
        createIfMissing: true,
        encrypt: false,
        backup: false,
        autoSync: true,
        kvStoreType: 1,
        securityLevel: 1,
      };
      CommonLog.info('[KvStoreModel] kvManager.getKVStore begin');
      self.kvManager.getKVStore(STORE_ID, options).then((store) => {
        CommonLog.info('[KvStoreModel] getKVStore success, kvStore=' + store + 'callback' + JSON.stringify(callback));
        self.kvStore = store;
        callback();
        CommonLog.info('[KvStoreModel] getKVStore end ');
      });
      CommonLog.info('[KvStoreModel] kvManager.getKVStore end');
    });
    CommonLog.info('[KvStoreModel] createKVManager end');
  }

  put(key, value) {
    CommonLog.info('[KvStoreModel] kvStore.put ' + key + '=' + JSON.stringify(value));
    this.kvStore.put(key, JSON.stringify(value)).then((data) => {
      CommonLog.info('[KvStoreModel] kvStore.put ' + key + ' finished, data=' + JSON.stringify(data));
    }).catch((err) => {
      CommonLog.error('[KvStoreModel] kvStore.put ' + key + ' failed, ' + JSON.stringify(err));
    });
  }

  setOffMessageReceivedListener(){
    CommonLog.info('[KvStoreModel] setOffMessageReceivedListener start');
    let self = this;
    this.createKvStore(() => {
      CommonLog.info('[KvStoreModel] kvStore.off start');
      CommonLog.info('[KvStoreModel] kvStore.off start' + JSON.stringify(self.kvStore));
      CommonLog.info('[KvStoreModel] kvStore.off start' + JSON.stringify(self.kvStore.off));
      self.kvStore.off('dataChange')
      CommonLog.info('[KvStoreModel] kvStore.off end');
    })
  }

  setOnMessageReceivedListener(msg, refreshdata) {
    CommonLog.info('[KvStoreModel] setOnMessageReceivedListener ' + msg);
    let self = this;
    this.createKvStore(() => {
      CommonLog.info('[KvStoreModel] kvStore.on(dataChange) begin');
      CommonLog.info('[KvStoreModel] kvStore.on(dataChange) self.kvStore ' + JSON.stringify(self.kvStore));
      CommonLog.info('[KvStoreModel] kvStore.on(dataChange) self.kvStore ' + JSON.stringify(self.kvStore.on));

      self.kvStore.on('dataChange', 2, (data) => {
        CommonLog.info('[KvStoreModel] dataChange, ' + JSON.stringify(data));
        CommonLog.info('[KvStoreModel] dataChange, insert ' + data.insertEntries.length + ' udpate '
        + data.updateEntries.length);
        let entries = data.insertEntries.length > 0 ? data.insertEntries : data.updateEntries;
        for (let i = 0; i < entries.length; i++) {
          if (entries[i].key === msg) {
            let value = entries[i].value.value;
            CommonLog.info('[KvStoreModel] Entries receive ' + msg + '=' + value);
            refreshdata(value);
            return;
          }
        }
      });
      CommonLog.info('[KvStoreModel] kvStore.on(dataChange) end');
    });
  }
}



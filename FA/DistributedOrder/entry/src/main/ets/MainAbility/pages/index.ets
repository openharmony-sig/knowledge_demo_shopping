/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License,Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * 首页面
 */

import router from '@system.router';
import { MenuListData } from '../model/MenuListData';
import prompt from '@system.prompt';
import { RemoteDevice, RemoteDeviceManager, RemoteDeviceStatus } from '../model/RemoteDeviceManager';
import CommonLog from '../model/CommonLog';
import { MenuData, Specialty, WinterNew, Classic, Soup, initializeOnStartup } from '../model/MenuData';
import featureAbility from '@ohos.ability.featureAbility';
import deviceInfo from '@ohos.deviceInfo'

@CustomDialog
struct DeviceDialog {
    private controller: CustomDialogController
    @Consume deviceList: RemoteDevice[]
    private invite: (remoteDevice: RemoteDevice) => void
    @State name: string = '本机'

    build() {
        Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center }) {
            if (this.deviceList.length == 0) {
                Text('无在线设备').textAlign(TextAlign.Center)
            } else {
                ForEach(this.deviceList, item => {
                    Text('本机')
                        .backgroundColor(this.name == '本机' ? '#02B175' : '#FFFFFF')
                        .margin({ top: '10lpx' })
                        .onClick(() => {
                            prompt.showToast({ message: 'Your own equipment ' })
                        })
                    Flex({
                        direction: FlexDirection.Column,
                        justifyContent: FlexAlign.Center,
                        alignItems: ItemAlign.Center
                    }) {
                        Text(item.deviceName)
                            .fontSize('30lpx')
                            .fontColor("#000000")
                            .height("40lpx")
                            .padding({ left: '10lpx', right: '10lpx' })
                            .backgroundColor(this.name == item.deviceName ? '#02B175' : '#FFFFFF')
                            .onClick(() => {
                                this.name = item.deviceName
                                this.invite(item)
                                this.controller.close()
                            })
                    }
                }, item => item.deviceId)
            }
        }
        .height('300lpx')
        .width('100%')

    }
}

@Component
struct MenuListItem {
    private menuItem: MenuData
    @Consume TotalMenu: any[];
    @Consume userName: string

    build() {
        Flex({ direction: FlexDirection.Row }) {

            Image(this.menuItem.imgSrc)
                .width('210lpx')
                .height('100%')
                .onClick(() => {
                    router.push({
                        uri: 'pages/detailedPage',
                        params: { menuItem: this.menuItem, TotalMenu: this.TotalMenu, userName: this.userName }
                    })
                })

            Flex({ direction: FlexDirection.Column }) {
                Text(this.menuItem.name)
                    .fontSize('32lpx')
                    .flexGrow(1)
                Text(this.menuItem.remarks)
                    .fontSize('22lpx')
                    .fontColor('#000000')
                    .opacity(0.6)
                    .flexGrow(1)
                Flex({ direction: FlexDirection.Row, alignItems: ItemAlign.Center }) {
                    Text('￥')
                        .fontSize('22lpx')
                    Text(this.menuItem.price.toString())
                        .fontSize('40lpx')
                    Text('/份')
                        .fontSize('22lpx')
                        .flexGrow(1)
                    Image($r("app.media.icon_reduce"))
                        .width('44lpx')
                        .height('44lpx')
                        .onClick(() => {
                            prompt.showToast({
                                message: "Reduce function  to be completed",
                                duration: 5000
                            })
                        })
                    Text(this.menuItem.quantity.toString())
                        .margin({ left: '15lpx', right: '15lpx' })
                    Image($r("app.media.icon_add"))
                        .width('44lpx')
                        .height('44lpx')
                        .margin({ right: '15lpx' })
                        .onClick(() => {
                            prompt.showToast({
                                message: "Increase function to be completed",
                                duration: 5000
                            })
                        })
                }
                .flexGrow(2)
            }
            .padding({ left: '5%', top: '5%' })
            .width('58%')
        }
        .margin({ top: '12lpx' })
        .borderRadius('16lpx')
        .height('180lpx')
        .backgroundColor('#FFFFFF')
    }
}

@Component
struct TotalInfo {
    @Consume TotalMenu: any[];
    private total: number = 0;
    private amount: number = 0;
    private remoteData: MenuListData

    aboutToAppear() {

    }

    getTotal() {
        let total = 0
        for (var index = 0; index < this.TotalMenu.length; index++) {
            total += this.TotalMenu[index].price * this.TotalMenu[index].quantity
        }
        this.total = total
        return total + ''
    }

    getAmount() {
        let amount = 0
        for (var index = 0; index < this.TotalMenu.length; index++) {
            amount += this.TotalMenu[index].quantity
        }
        this.amount = amount
        return amount + ''
    }

    aboutToDisappear() {
        this.remoteData.setOffMessageReceivedListener();
    }

    build() {
        Flex({ direction: FlexDirection.Row, alignItems: ItemAlign.Center }) {
            Stack({ alignContent: Alignment.Center }) {
                Image($r("app.media.icon_cart"))
                    .width('96lpx')
                    .height('96lpx')
                    .margin({ left: '22lpx' })
                Text(this.getAmount())
                    .backgroundColor('#F84747')
                    .borderRadius('30plx')
                    .fontSize('24plx')
                    .textAlign(TextAlign.Center)
                    .fontColor('#FFFFFF')
                    .width('50lpx')
                    .height('50lpx')
                    .margin({ left: '100lpx', bottom: '85lpx' })
            }
            .width('150lpx')
            .height('150lpx')

            Text('￥')
                .fontSize('22lpx')
                .fontColor('#006A3A')
                .margin({ left: '22lpx' })
            Text(this.getTotal())
                .fontSize('40lpx')
                .fontColor('#006A3A')
                .flexGrow(1)
            Text('点好了')
                .height('100%')
                .width('35%')
                .fontColor('#FFFFFF')
                .backgroundColor('#F84747')
                .textAlign(TextAlign.Center)
        }
        .width('100%')
        .height('10%')
        .backgroundColor('#FFFFFF')
        .onClick(() => {
            this.remoteData.put("menu_list", this.TotalMenu)
            router.push({
                uri: 'pages/menuAccount',
                params: { TotalMenu: this.TotalMenu }
            })
        })
    }
}

@Component
struct MenuHome {
    private specialty: any[]
    private winterNew: any[]
    private classic: any[]
    private soup: any[]
    private menuItems: MenuData[]
    private titleList = ['招牌菜', '冬季新品', '下饭菜', '汤品']
    @State name: string = '招牌菜'

    build() {
        Flex({ direction: FlexDirection.Row, alignItems: ItemAlign.Start }) {
            Flex({ direction: FlexDirection.Column, justifyContent: FlexAlign.SpaceAround }) {
                ForEach(this.titleList, item => {
                    Flex({ alignItems: ItemAlign.Center, justifyContent: FlexAlign.Start }) {
                        Text(item)
                            .fontSize('24lpx')
                    }
                    .padding({ left: '24lpx' })
                    .backgroundColor(this.name == item ? '#1A006A3A' : '#FFFFFF')
                    .height('160lpx')
                    .onClick(() => {
                        this.name = item
                        if (this.name == '招牌菜') {
                            this.menuItems = initializeOnStartup(this.specialty);
                        }
                        else if (this.name == '冬季新品') {
                            this.menuItems = initializeOnStartup(this.winterNew);
                        }
                        else if (this.name == '下饭菜') {
                            this.menuItems = initializeOnStartup(this.classic);
                        }
                        else if (this.name == '汤品') {
                            this.menuItems = initializeOnStartup(this.soup);
                        }
                    })
                }, item => item)
            }
            .width('20%')
            .backgroundColor('#FFFFFF')

            Flex({ direction: FlexDirection.Column }) {
                Text(this.name)
                    .fontSize('32lpx')
                    .fontWeight(FontWeight.Bold)
                    .opacity(0.4)
                    .height('8%')
                Scroll() {
                    Column() {
                        List() {
                            ForEach(this.menuItems, item => {
                                ListItem() {
                                    MenuListItem({ menuItem: item })
                                }
                            }, item => item.id.toString())
                        }
                    }
                }
                .height('92%')
            }
            .margin({ left: '10lpx' })
            .width('75%')

        }
        .height('50%')
    }
}

@Component
struct MemberInfo {
    @Consume userImg: Resource
    @Consume userName: string
    @Provide deviceList: RemoteDevice[] = []
    private remoteDm: RemoteDeviceManager = new RemoteDeviceManager(this.deviceList)
    private DeviceDialog: CustomDialogController = new CustomDialogController({
        builder: DeviceDialog({
            invite: (remoteDevice) => this.inviteDevice(remoteDevice)
        }),
        autoCancel: true
    })

    aboutToAppear() {
        this.remoteDm.refreshRemoteDeviceList()
        this.remoteDm.startDeviceDiscovery()
    }

    aboutToDisappear() {
        CommonLog.info('aboutToDisappear');
        this.remoteDm.stopDeviceDiscovery()
    }

    inviteDevice(remoteDevice: RemoteDevice) {
        if (remoteDevice.status == RemoteDeviceStatus.ONLINE) {
            this.startAbility(remoteDevice.deviceId)
        }
        else {
            this.remoteDm.authDevice(remoteDevice).then(() => {
                prompt.showToast({ message: "Invite success! deviceName=" + remoteDevice.deviceName })
                this.startAbility(remoteDevice.deviceId)
            }).catch(() => {
                prompt.showToast({ message: "Invite fail!" })
            })
        }
    }

    startAbility(deviceId: string) {
        CommonLog.info('featureAbility.startAbility deviceId=' + deviceId);
        var str1 = {
            "StartAbilityParameter": {
                "want": {
                    "deviceId": deviceId,
                    "bundleName": "com.distributed.order",
                    "abilityName": "com.distributed.order.MainAbility",
                    "parameters": {},
                }
            }
        };
        featureAbility.startAbility(str1.StartAbilityParameter)
            .then((data) => {
                CommonLog.info('Operation successful. Data: ' + JSON.stringify(data))
            }).catch(err => {
            CommonLog.error('Operation fail. Data: ' + JSON.stringify(err))
        })
    }

    build() {
        Flex({ direction: FlexDirection.Column }) {
            Flex({ direction: FlexDirection.Row, alignItems: ItemAlign.Center }) {
                Image(this.userImg)
                    .width('96lpx')
                    .height('96lpx')
                    .margin({ right: '18lpx' })
                Text(this.userName)
                    .fontSize('36lpx')
                    .fontWeight(FontWeight.Bold)
                    .flexGrow(1)
                Image($r("app.media.icon_share"))
                    .width('64lpx')
                    .height('64lpx')
            }
            .onClick(() => {
                this.DeviceDialog.open()
            })
            .layoutWeight(1)
            .padding({ left: '48lpx', right: '48lpx' })

            Flex({ direction: FlexDirection.Row, alignItems: ItemAlign.Center }) {
                Column() {
                    Text('124')
                        .fontSize('40lpx')
                        .margin({ bottom: '24lpx' })
                    Text('积分')
                        .fontSize('22lpx')
                        .opacity(0.4)
                }
                .flexGrow(1)

                Column() {
                    Text('0')
                        .fontSize('40lpx')
                        .margin({ bottom: '24lpx' })
                    Text('优惠劵')
                        .fontSize('22lpx')
                        .opacity(0.4)
                }
                .flexGrow(1)

                Column() {
                    Image($r("app.media.icon_member"))
                        .width('48lpx')
                        .height('48lpx')
                        .margin({ bottom: '24lpx' })
                    Text('会员码')
                        .fontSize('22lpx')
                        .fontColor('#000000')
                        .opacity(0.4)
                }
                .flexGrow(1)
            }
            .layoutWeight(1)
        }
        .width('93%')
        .height('25%')
        .borderRadius('16lpx')
        .backgroundColor('#FFFFFF')
        .margin({ top: '24lpx', bottom: '32lpx' })

    }
}

@Component
struct PageInfo {
    build() {
        Flex() {
            Text('点单页')
                .fontSize('36lpx')
        }
        .padding({ left: '48lpx', top: '28lpx' })
        .width('100%')
        .height('10%')
        .backgroundColor('#FFFFFF')
    }
}

@Entry
@Component
struct Index {
    @Provide userImg: Resource = $r("app.media.icon_user")
    @Provide userName: string = 'Sunny'
    @Provide TotalMenu: any[] = [];
    @State specialty: any[] = Specialty
    @State winterNew: any[] = WinterNew
    @State classic: any[] = Classic
    @State soup: any[] = Soup
    @State menuItems: MenuData[] = initializeOnStartup(this.specialty)
    private remoteData: MenuListData = new MenuListData()

    aboutToAppear() {
        let context = featureAbility.getContext()
        context.requestPermissionsFromUser(['ohos.permission.DISTRIBUTED_DATASYNC'], 666, function (result) {
            console.info(`[IndexPage] grantPermission,requestPermissionsFromUser,result.requestCode=${result.code}`)
        })
        CommonLog.info('==serial===' + deviceInfo.serial);
        if ("1234567890" == deviceInfo.serial) {
            this.userImg = $r("app.media.icon_user")
            this.userName = 'Sunny'
        } else {
            this.userImg = $r("app.media.icon_user_another")
            this.userName = 'Jenny'
        }

        this.remoteData.setOnMessageReceivedListener('menu_list', (res) => {
            CommonLog.info("======dataChange==" + res);
            let self = this;
            var data = JSON.parse(res);
            if (data.length > 0) {
                var list = [];
                for (var i = 0; i < data.length; i++) {
                    list[i] = data[i];
                }
                router.push({
                    uri: 'pages/menuAccount',
                    params: { TotalMenu: list }
                })
            }
        });
    }

    onPageShow() {
        this.initData()
    }

    initData() {
        let arr = router.getParams().TotalMenu as Array<any>
        CommonLog.info("initData" + JSON.stringify(router.getParams().TotalMenu))
        if (router.getParams().TotalMenu) {
            var itemData: any = router.getParams().TotalMenu
            var listData = [];
            for (var i = 0; i < itemData.length; i++) {
                listData[i] = itemData[i];
                if (listData[i].name == '红烧排骨') {
                    this.specialty[0].quantity = listData[i].quantity
                    this.classic[3].quantity = listData[i].quantity
                }
                else if (listData[i].name == '茶油炒土鸡') {
                    this.specialty[1].quantity = listData[i].quantity
                    this.classic[3].quantity = listData[i].quantity
                }
                else if (listData[i].name == '黄焖禾花鱼') {
                    this.specialty[2].quantity = listData[i].quantity
                    this.classic[3].quantity = listData[i].quantity
                }
                else if (listData[i].name == '香煎鲫鱼') {
                    this.specialty[3].quantity = listData[i].quantity
                    this.classic[3].quantity = listData[i].quantity
                }
                else if (listData[i].name == '冬笋炒腊肉') {
                    this.winterNew[0].quantity = listData[i].quantity
                }
                else if (listData[i].name == '粉皮羊肉') {
                    this.winterNew[1].quantity = listData[i].quantity
                }
                else if (listData[i].name == '莲藕猪骨汤') {
                    this.soup[0].quantity = listData[i].quantity
                }
            }
            this.menuItems = initializeOnStartup(this.specialty);

            this.TotalMenu = listData;
            CommonLog.info("initData2:" + this.TotalMenu.length)
        }
    }

    build() {
        Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Start }) {
            PageInfo();
            MemberInfo();
            MenuHome({
                specialty: this.specialty,
                winterNew: this.winterNew,
                classic: this.classic,
                soup: this.soup,
                menuItems: this.menuItems
            });
            TotalInfo({ remoteData: this.remoteData });
        }
        .width('100%')
        .height('100%')
        .backgroundColor('#F1F3F5')
    }
}
/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License,Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * 购物车列表分布式数据库
 */

import CommonLog from './CommonLog.ets';
import distributedData from '@ohos.data.distributedData';

let NextId = 0;
export class ShoppingCartsInfo {
  id: string;
  title: string;
  flag:number
  price: number;
  imgSrc: Resource;

  constructor(title: string, flag: number, price: number, imgSrc: Resource) {
    this.id = `${NextId++}`;
    this.title = title;
    this.flag = flag;
    this.price = price;
    this.imgSrc = imgSrc;
  }
}

export function initShoppingCartsGoods(ShoppingCartsGoods: any[]): Array<ShoppingCartsInfo> {
  let GoodsDataArray: Array<ShoppingCartsInfo> = []
  ShoppingCartsGoods.forEach(item => {
    console.log(item.title);
    GoodsDataArray.push(new ShoppingCartsInfo(item.title, item.flag,item.price, item.imgSrc));
  })
  return GoodsDataArray;
}

export default class ShoppingCartData {
  callback;
  kvManager;
  kvStore;
  dataItem;

  constructor() {
  }

  createManager(callback) {
    if (typeof (this.kvManager) === 'undefined') {
      let self = this;
      try {
        const kvManagerConfig = {
          bundleName: 'com.open.DistributedShopping',
          userInfo: {
            userId: '0',
            userType: 0
          }
        }
        distributedData.createKVManager(kvManagerConfig).then((manager) => {
          console.log("createKVManager success");
          self.kvManager = manager;
          self.getKVStore(callback);
        }).catch((err) => {
          console.log("createKVManager err: " + JSON.stringify(err));
        });
      } catch (e) {
        console.log("An unexpected error occurred. Error:" + e);
      }
    }
    else {
      this.getKVStore(callback);
    }
  }

  getKVStore(callback) {
    CommonLog.info('getKVStore');
    this.callback = callback;
    if (this.kvManager == undefined) {
      console.error(' createManager has not initialized');
      this.callback();
      return;
    }
    try {
      const options = {
        createIfMissing: true,
        encrypt: false,
        backup: false,
        autoSync: true,
        kvStoreType: 1,
        securityLevel: 3,
      };

      let self = this;
      this.kvManager.getKVStore('shopping', options).then((store) => {
        console.log("getKVStore success");
        self.kvStore = store;
        self.monitorData(callback)
      }).catch((err) => {
        console.log("getKVStore err: " + JSON.stringify(err));
      });
    } catch (e) {
      console.log("An unexpected error occurred. Error:" + e);
    }
  }

  monitorData(callback) {
    CommonLog.info('monitorData');
    this.callback = callback;
    if (this.kvStore == undefined) {
      console.error(' createManager has not initialized');
      this.callback();
      return;
    }
    let self = this;
    this.kvStore.on('dataChange', 2, function (data) {
      console.log("dataChange callback call data: " + JSON.stringify(data));

      if (data.insertEntries.length > 0) {
        console.log("dataChange key: " + data.insertEntries[0].key);
        if (data.insertEntries[0].key.match("shopping_cart")) {
          console.log("dataChange insertEntries: " + JSON.stringify(data.insertEntries[0].value.value));
          var tempInsertObj = JSON.parse(data.insertEntries[0].value.value);
          self.dataItem = {
            "flag":tempInsertObj.flag,
            "title": tempInsertObj.title,
            "price": tempInsertObj.price,
            "imgSrc": tempInsertObj.imgSrc
          }
        }
        self.callback()
      }

      else if (data.updateEntries.length > 0) {
        var tempUpdateObj = JSON.parse(data.updateEntries[0].value.value);
        console.log("dataChange tempUpdateObj: " +  tempUpdateObj.length);
        var list = [];
        for (var i = 0; i < tempUpdateObj.length; i++) {
           list[i] = tempUpdateObj[i];
        }
        self.dataItem = list;
        self.callback();
      }
    });
  }

  putData(key, value) {
    try {
      this.kvStore.put(JSON.stringify(key), JSON.stringify(value)).then((data) => {
        console.log("put success: " + JSON.stringify(data));
      }).catch((err) => {
        console.log("put err: " + JSON.stringify(err));
      });
    } catch (e) {
      console.log("An unexpected error occurred. Error:" + e);
    }
  }
}



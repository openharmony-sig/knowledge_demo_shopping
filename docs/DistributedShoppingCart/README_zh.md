# [OpenHarmony分布式购物车](../../FA/DistributedShoppingCart)

## 一、简介

#### 1.样例效果

分布式购物车demo 模拟的是我们购物时参加满减活动，进行拼单的场景；实现两人拼单时，其他一人添加商品到购物车，另外一人购物车列表能同步更新，且在购物车列表页面结算时，某一人结算对方也能实时知道结算金额和优惠金额。整个操作效果分为3个小动画，

- 拉起对方用户

![show](media/pull_demo.gif)

- 添加商品到购物车列表

![show](media/merge_goods.gif)

- 购物车列表勾选

![show](media/tick_goods.gif)

- demo效果（HH-SCDAYU200）

![show](media/3568.gif)

#### 2.设计OpenHarmony技术特性

- eTS UI
- 分布式调度
- 分布式数据管理

####  3.支持OpenHarmony版本

OpenHarmony 3.0 LTS 、OpenHarmony 3.1 Beta

#### 4.支持开发板

- 润和HiSpark Taurus AI Camera(Hi3516d)开发板套件（OpenHarmony 3.0 LTS 、OpenHarmony 3.1 Beta）
- 润和大禹系列HH-SCDAYU200开发板套件（OpenHarmony 3.1 Beta，该开发板无3.0 LTS版本）

## 二、快速上手

#### 1.标准设备环境准备

以润和HiSpark Taurus AI Camera(Hi3516d)开发板套件为例

- [获取OpenHarmony源码](https://gitee.com/openharmony/docs/blob/master/zh-cn/release-notes/OpenHarmony-v3.0.1-LTS.md)OpenHarmony 3.0 LTS
- [获取OpenHarmony源码](https://gitee.com/openharmony/docs/blob/master/zh-cn/release-notes/OpenHarmony-v3.1-beta.md)OpenHarmony 3.1 Beta；
- [源码编译](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.2-Beta4/zh-cn/device-dev/quick-start/quickstart-ide-3568-build.md)
- [开发板烧录](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.2-Beta4/zh-cn/device-dev/quick-start/quickstart-ide-3516-burn.md)

以润和大禹系列HH-SCDAYU200开发板套件为例

- [润和RK3568开发板标准设备上手-HelloWorld](https://gitee.com/openharmony-sig/knowledge_demo_temp/tree/master/docs/rk3568_helloworld)

#### 2.应用编译环境准备

- 下载DevEco Studio [下载地址](https://developer.harmonyos.com/cn/develop/deveco-studio#download_beta)；请下载 DevEco Studio 3.0 Beta2 for HarmonyOS 版本（demo源码是基于该版本IDE开发）
- 配置SDK，参考 [配置OpenHarmony-SDK](https://gitee.com/openharmony/docs/blob/OpenHarmony-v3.1-Beta/zh-cn/application-dev/quick-start/configuring-openharmony-sdk.md)
- DevEco Studio 点击File -> Open 导入本下面的代码工程DistributedShoppingCart

#### 3.项目下载和导入

项目地址：https://gitee.com/openharmony-sig/knowledge_demo_shopping/tree/master/FA/DistributedShoppingCart

1）git下载

```
git clone https://gitee.com/openharmony-sig/knowledge_demo_shopping.git  --depth=1
```

2）项目导入

打开DevEco Studio,点击File->Open->下载路径/FA/Shopping/DistributedShoppingCart


#### 4.安装应用

- [配置应用签名信息](https://gitee.com/openharmony/docs/blob/OpenHarmony-v3.1-Beta/zh-cn/application-dev/quick-start/configuring-openharmony-app-signature.md)

- [安装应用](https://gitee.com/openharmony/docs/blob/OpenHarmony-v3.1-Beta/zh-cn/application-dev/quick-start/installing-openharmony-app.md) 如果IDE没有识别到设备就需要通过命令安装，如下

  打开**OpenHarmony SDK路径 \toolchains** 文件夹下，执行如下hdc_std命令，其中**path**为hap包所在绝对路径。

  ```text
  hdc_std install -r path\entry-debug-standard-ark-signed.hap
  ```

  体验分布式购物车时，需要两个开发板，连接同一个wifi或使用网线连接并配置同一网段IP地址

  ```
  hdc shell ifconfig eth0 192.168.1.111 netmask 255.255.255.0  hdc shell ifconfig eth0 192.168.1.222 netmask 255.255.255.0
  ```

**PS**环境准备，源码下载，编译，烧录设备，应用部署的完整步骤请参考[这里](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.2-Beta4/zh-cn/application-dev/quick-start/start-overview.md)

## 三、关键代码解读

#### 1.目录结构

```
├─entry\src\main
│          │  config.json  应用配置文件
│          │  
│          ├─ets
│          │  └─MainAbility
│          │      │  app.ets  ets应用程序主入口
│          │      │  
│          │      ├─model
│          │      │      ArsData.ets     // 初始化我的页面数据
│          │      │      CommonLog.ets   // 日志类
│          │      │      GoodsData.ets   // 初始化商品信息数据类
│          │      │      MenuData.ets    // 初始化我的页面数据类
│          │      │      RemoteDeviceManager.ets  // 分布式拉起设备管理类
│          │      │      ShoppingCartDistributedData.ets  // 加入购物车分布式数据库
│          │      │      TotalSelectedDistributedData.ets // 结算购物车分布式数据库
│          │      │      
│          │      └─pages
│          │              DetailPage.ets   // 商品详情页面
│          │              HomePage.ets     // 应用首页
│          │              MyPage.ets       // 我的页面
│          │              ShoppingCartListPage.ets  // 购物车列表页面
│     └─resources // 静态资源目录
│         ├─base
│         │  ├─element
│         │  ├─graphic
│         │  ├─layout
│         │  ├─media // 存放媒体资源
│         │  └─profile
│         └─rawfile
```

#### 2.日志查看方法

```
hdc_std shell
hilog | grep shopping 
```

#### 3.关键代码

- UI界面，设备流转：HomePages.ets
- 设备管理：RemoteDeviceManager.ets
- 分布式数据管理：ShoppingCartDistributedData.ets  TotalSelectedDistributedData.ets

## 四、如何从零开发分布式购物车

[从零开发分布式购物车](quick_develop.md)

## 五、参考链接

- [OpenHarmony基于TS扩展的声明式开发范式](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/reference/arkui-ts/Readme-CN.md)
- [OpenHarmony应用接口](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/reference/apis/Readme-CN.md)
- [OpenHarmonyETS开发FAQ](https://gitee.com/Cruise2019/team_x/blob/master/homework/ets_quick_start/ETS%E5%BC%80%E5%8F%91FAQ.md)

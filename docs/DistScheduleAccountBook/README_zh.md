# [OpenHarmony分布式账本](../../FA/MyAccountBook)

## 一、简介

#### 1.样例效果

本Demo基于OpenHarmony3.1 Beta，使用ETS语言编写的应用。该样例展示了设备认证，分布式流转，分布式数据管理的能力，新设备通过设备认证后，可以通过分布式流转功能拉起远程设备，通过分布式数据管理能力同步两台设备之间的数据。

新设备需要认证,获取同一个局域网内的设备ID，并拉起应用（Hi3516d）

&nbsp;![laqi](./media/laqi.gif)

添加数据并在另一台设备显示该数据（Hi3516d）

&nbsp;![accountbook](./media/accountbook.gif)

拉起应用，添加数据并在另一台设备显示该数据（HH-SCDAYU200）

&nbsp;![accountbook](./media/zhangdang.gif)

#### 2.涉及OpenHarmony技术特性

- eTS UI
- 分布式调度
- 分布式数据管理

#### 3.支持OpenHarmony版本

OpenHarmony 3.0 LTS、OpenHarmony 3.1 Beta。

#### 4.支持开发板

- 润和HiSpark Taurus AI Camera(Hi3516d)开发板套件
- 润和大禹系列HH-SCDAYU200开发板套件（OpenHarmony 3.1 Beta，该开发板无3.0 LTS版本）

## 二、 快速上手

#### 1.标准设备环境准备

润和HiSpark Taurus AI Camera(Hi3516d)开发板套件:

- [Hi3516DV300开发板标准设备HelloWorld](https://gitee.com/openharmony-sig/knowledge_demo_temp/blob/master/docs/hi3516_dv300_helloworld/README.md)，参考环境准备、编译和烧录章节;

润和大禹系列HH-SCDAYU200开发套件：

- [润和RK3568开发板标准设备上手-HelloWorld](https://gitee.com/openharmony-sig/knowledge_demo_temp/blob/master/docs/rk3568_helloworld/README.md);

#### 2.应用编译环境准备

- 下载DevEco Studio [下载地址](https://developer.harmonyos.com/cn/develop/deveco-studio#download_beta)；请下载 DevEco Studio 3.0 Beta2 版本（demo源码是基于该版本IDE开发）
- 配置SDK，参考  [配置OpenHarmony-SDK](https://gitee.com/openharmony/docs/blob/OpenHarmony-v3.1-Beta/zh-cn/application-dev/quick-start/configuring-openharmony-sdk.md);
- DevEco Studio 点击File -> Open 导入本下面的代码工程MyAccountBook

#### 3.项目下载和导入

项目地址：https://gitee.com/openharmony-sig/knowledge_demo_shopping/tree/master/FA/MyAccountBook

1）git下载

```shell
git clone https://gitee.com/openharmony-sig/knowledge_demo_shopping.git  --depth=1
```

2）项目导入

打开DevEco Studio,点击File->Open->下载路径/FA/Shopping/MyAccountBook

#### 4.安装应用

- [配置应用签名信息](https://gitee.com/openharmony/docs/blob/OpenHarmony-v3.1-Beta/zh-cn/application-dev/quick-start/configuring-openharmony-app-signature.md)

- [安装应用](https://gitee.com/openharmony/docs/blob/OpenHarmony-v3.1-Beta/zh-cn/application-dev/quick-start/installing-openharmony-app.md)

  打开**OpenHarmony SDK路径 \toolchains** 文件夹下，执行如下hdc_std命令，其中**path**为hap包所在绝对路径。

  ```text
  hdc_std install -r path\entry-debug-standard-ark-signed.hap
  ```

**PS**分布式流转流转时，需要多个开发板，连接同一个wifi或使用网线连接

## 三、关键代码解读

#### 1.目录结构

```shell
.
|-- Readme.md
|-- build.gradle
|-- entry
|   `-- src
|       `-- main
|           |-- config.json  //应用配置
|           |-- ets
|           |   `-- MainAbility
|           |       |-- app.ets
|           |       |-- model
|           |       |   `-- PayDataModels.ets //封装数据类
|           |       |-- pages
|           |       |   |-- CommonLog.ets //日志类
|           |       |   |-- DeviceListDialog.ets //设备列表自定义弹窗组件
|           |       |   |-- RemoteDataManager.ets //分布式数据管理
|           |       |   |-- RemoteDeviceManager.ets  //设备认证管理
|           |       |   |-- add.ets   //添加数据页面
|           |       |   |-- constants.ets  // 常用数据存放类
|           |       |   `-- index.ets  //首页
|           |       `-- picture
|           `-- resources
|               |-- base
|               |   |-- element
|               |   |-- graphic
|               |   |-- layout
|               |   |-- media //存放媒体资源
|               |   `-- profile
|               |-- phone
|               |   -- media
|               -- rawfile

```

#### 2.日志查看方法

```shell
hdc_std shell
hilog | grep MyOpenHarmonyAccountBook  
```

#### 3.关键代码

- UI界面，设备流转：index.ets
- UI界面，添加数据：add.ets
- 设备认证管理: RemoteDeviceManager.ets
- 分布式数据管理: RemoteDataManager.ets

## 四、如何从零开发分布式账本

[从零开发分布式账本](quick_develop.md)

## 五、参考链接

- [OpenHarmony基于TS扩展的声明式开发范式](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/reference/arkui-ts/Readme-CN.md)
- [OpenHarmony应用接口](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/reference/apis/Readme-CN.md)
- [OpenHarmonyETS开发FAQ](https://gitee.com/Cruise2019/team_x/blob/master/homework/ets_quick_start/ETS%E5%BC%80%E5%8F%91FAQ.md)

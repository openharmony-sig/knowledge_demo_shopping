# knowledge_demo_shopping

## 简介

基于购物消费场景打造的从点单消费到购物记账样例，开发者可以体验各种购物消费的开发样例，享受万物互联带来的便利。

![shopping view](./media/shopping_view.png)

## 样例汇总

### 连接模组类应用

### 带屏IoT设备应用

### Camera应用

### 标准系统应用

+ [OpenHarmony 分布式菜单](docs/DistributedOrder/README_zh.md)
+ [OpenHarmony 分布式购物车](docs/DistributedShoppingCart/README_zh.md)
+ [OpenHarmony 分布式账本](docs/DistScheduleAccountBook/README_zh.md)

## 参考资料

+ [OpenHarmony官网](https://www.openharmony.cn/)
+ [OpenHarmony知识体系仓库](https://gitee.com/openharmony-sig/knowledge)
+ [OpenHarmony知识体系议题申报](https://docs.qq.com/sheet/DUUNpcWR6alZkUmFO)
+ [OpenHarmony知识体系例会纪要](https://gitee.com/openharmony-sig/sig-content/tree/master/knowlege/meetings)
+ [OpenHarmony开发样例共建说明](https://gitee.com/openharmony-sig/knowledge/blob/master/docs/co-construct_demos/README_zh.md)
+ [OpenHarmony知识体系-智能家居](https://gitee.com/openharmony-sig/knowledge_demo_smart_home)
+ [OpenHarmony知识体系-智能出行场景](https://gitee.com/openharmony-sig/knowledge_demo_travel)
+ [OpenHarmony知识体系-影音娱乐场景](https://gitee.com/openharmony-sig/knowledge_demo_entainment)
